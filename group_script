#!/bin/sh
#
# Copyright (C) 2012-2021 Tuomo Soini <tis@foobar.fi>
#
# License: GPLv2
#

#
# event handling script for use with lsm groups
#

STATE=${1}
NAME=${2}
CHECKIP=${3}
DEVICE=${4}
WARN_EMAIL=${5}
REPLIED=${6}
WAITING=${7}
TIMEOUT=${8}
REPLY_LATE=${9}
CONS_RCVD=${10}
CONS_WAIT=${11}
CONS_MISS=${12}
AVG_RTT=${13}
SRCIP=${14}
PREVSTATE=${15}
TIMESTAMP=${16}
MIN_RTT=${17}
MAX_RTT=${18}

if [ -z "${WARN_EMAIL}" ] ; then
    exit 0
fi

DATE=$(date +'%Y-%m-%d %H:%M:%S %Z' --date=@${TIMESTAMP})
HOSTNAME=$(hostname)

cat <<EOM | mail -s "Foolsm: ${NAME} ${STATE}" ${WARN_EMAIL}

Hi,

Your connection ${NAME} has changed its state to ${STATE}
on ${HOSTNAME} at ${DATE}.

Following parameters were passed:
prevstate    = ${PREVSTATE}
newstate     = ${STATE}
name         = ${NAME}
host         = ${HOSTNAME}
warn_email   = ${WARN_EMAIL}
device       = ${DEVICE}

Packet statuses are not available for groups.

BR,
Your Foolsm installation

EOM

exit 0
